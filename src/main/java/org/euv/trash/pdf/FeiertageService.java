package org.euv.trash.pdf;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FeiertageService {
	
	@Autowired
	AppConfig appConfig;
	
	@Cacheable("Feiertag")
	public List<Feiertag> getFeiertage(int jahr) {
		RestTemplate restTemplate = new RestTemplate();
		
		LinkedList<Feiertag> feiertags = new LinkedList<Feiertag>();
		
		ResponseEntity<String> response = restTemplate.getForEntity(appConfig.getFeiertagBaseUrl() +"?nur_land=NW&jahr="+ Integer.toString(jahr), String.class);
		if(response.getStatusCode() == HttpStatus.OK) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				JsonNode root = mapper.readTree(response.getBody());
				log.trace(root.toPrettyString());
				Iterator<Entry<String, JsonNode>> i = root.fields();
				while(i.hasNext()) {
					Feiertag feiertag = new Feiertag();
					Entry<String, JsonNode> e = i.next();
					log.trace("Entry: {}: {}", e.getKey(), e.getValue());
					feiertag.setName(e.getKey());
					try {
						feiertag.setDatum(LocalDate.parse(e.getValue().get("datum").asText()));
					}
					catch(NullPointerException npe) {
						log.warn("Could not set datum for {}", feiertag.getName());
					}
					try {
						feiertag.setHinweis(e.getValue().get("hinweis").asText());
					}
					catch(NullPointerException npe) {
						log.warn("Could not set hinweis for {}", feiertag.getName());
					}
					
					log.trace("Feiertag: {}", feiertag.toString());
					feiertags.add(feiertag);
				}
			} catch(JsonProcessingException e) {
				log.error("Catch exception", e);
			}
		}
		
		return feiertags;
	}
}
