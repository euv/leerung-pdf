package org.euv.trash.pdf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "app")
@Getter
@Setter
public class AppConfig {
	private String feiertagBaseUrl = "https://feiertage-api.de/api/";
	private String leerungBaseUrl = "https://api.euv-stadtbetrieb.de/leerung/";
	private String umweltbrummiBaseUrl = "https://api.euv-stadtbetrieb.de/umweltbrummi/";
	private String webapiBaseUrl = "https://api.euv-stadtbetrieb.de/web-api/";
}
