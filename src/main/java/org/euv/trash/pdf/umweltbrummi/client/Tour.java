package org.euv.trash.pdf.umweltbrummi.client;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tour {
	UUID id;
	
	String name;
	String beschreibung;
	
	Date gueltigVon;
	Date gueltigBis;
	
	Integer sort;
	Integer icon;
	
	UUID[] subTours;
}
