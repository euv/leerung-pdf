package org.euv.trash.pdf.umweltbrummi.client;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubTour {
	UUID id;
	String name;
	String beschreibungText;
	Integer sort;
	String beschreibung;
	SubTourDates[] subTourDates;
}
