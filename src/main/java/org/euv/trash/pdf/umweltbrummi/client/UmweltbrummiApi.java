package org.euv.trash.pdf.umweltbrummi.client;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.euv.trash.pdf.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UmweltbrummiApi {
	
	@Autowired
	AppConfig appConfig;
	
	public Tour[] getTour() {
		RestTemplate restTemplate = new RestTemplateBuilder().build();
		restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(appConfig.getUmweltbrummiBaseUrl()));

		Tour[] tours = null;
		ResponseEntity<Tour[]> result = restTemplate.getForEntity("/v1/tour", Tour[].class);
		log.debug("getTour status: {}", result.getStatusCodeValue());
		if(result.getStatusCode()==HttpStatus.OK) {
			tours = result.getBody();
		}
		
		return tours;
	}
	
	public SubTour getSubTour(UUID id) {
		RestTemplate restTemplate = new RestTemplateBuilder().build();
		restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(appConfig.getUmweltbrummiBaseUrl()));

		Map<String, String> params = new HashMap<String, String>();
		params.put("id", id.toString());
		
		SubTour subTour = null;
		ResponseEntity<SubTour> result = restTemplate.getForEntity("/v1/subtour/{id}", SubTour.class, params);
		log.debug("getSubTour status: {}", result.getStatusCodeValue());
		if(result.getStatusCode()==HttpStatus.OK) {
			subTour = result.getBody();
		}
		
		return subTour;
	}
}
