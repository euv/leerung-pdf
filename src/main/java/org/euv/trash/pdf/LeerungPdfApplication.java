package org.euv.trash.pdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan("org.euv.trash.pdf")
public class LeerungPdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeerungPdfApplication.class, args);
	}

}
