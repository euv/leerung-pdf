package org.euv.trash.pdf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix="app")
public class Config {
	String piwikUrl;
	Integer piwikSiteId;
	String piwikAuthToken;
	String piwikProxyServer;
	Integer piwikProxyPort;
}
