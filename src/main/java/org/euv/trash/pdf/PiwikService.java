package org.euv.trash.pdf;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import jakarta.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.piwik.java.tracking.CustomVariable;
import org.piwik.java.tracking.PiwikRequest;
import org.piwik.java.tracking.PiwikTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PiwikService {

	@Autowired
	Config config;
	
	private PiwikTracker piwikTracker = null;
	
	public PiwikService() {
		super();
	}
	
	private PiwikTracker getPiwikTracker() {
		if(piwikTracker==null) {
			if(config.getPiwikProxyServer()!=null && config.getPiwikProxyPort()!=null) {
				piwikTracker = new PiwikTracker(config.getPiwikUrl(), config.getPiwikProxyServer(), config.getPiwikProxyPort());
			} else {
				piwikTracker = new PiwikTracker(config.getPiwikUrl());
			}
		}
		return piwikTracker;
	}
	
	private PiwikRequest buildRequest(String url, HttpServletRequest http) throws MalformedURLException {
		log.debug("Visitor IP: {}", http.getRemoteAddr());
		PiwikRequest request = new PiwikRequest(config.getPiwikSiteId(), new URL(url));
		request.setAuthToken(config.getPiwikAuthToken());
		request.setVisitorIp(http.getRemoteAddr());
		request.setResponseAsImage(false);
		return request;
	}
	
	private void sendRequest(PiwikRequest request) throws IOException, InterruptedException, ExecutionException, TimeoutException {
		Future<HttpResponse> resp = getPiwikTracker().sendRequestAsync(request);
		log.debug("RESP: {}", resp.get(10, TimeUnit.SECONDS));
	}
	
	

	
	
	public void logCreatePdf(long haId, String userInfo, Optional<Integer> jahr, long time, HttpServletRequest http) {
		String url = http.getRequestURL().toString() + (http.getQueryString()==null ? "" : "?"+ http.getQueryString());
		log.debug("URL: {} / {}", url, time);
		try {
			PiwikRequest request = buildRequest(url, http);
			request.setActionTime(time);
			if(userInfo==null) {
				request.setUserId(Long.toString(haId));
			} else {
				request.setUserId(userInfo +" ["+Long.toString(haId)+"]");
			}
			request.setActionName("Get PDF");
			
			if(jahr.isPresent()) {
				request.setCustomTrackingParameter("4", jahr.get());
			}
			
			sendRequest(request);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void logError(long haId, Optional<Integer> jahr, Exception e, long time, HttpServletRequest http) {
		String url = http.getRequestURL().toString() + (http.getQueryString()==null ? "" : "?"+ http.getQueryString());
		log.debug("URL: {} / {}", url, time);
		try {
			PiwikRequest request = buildRequest(url, http);
			request.setActionTime(time);
			request.setUserId(Long.toString(haId));
			request.setActionName("Error in get PDF");

			if(jahr.isPresent()) {
				request.setPageCustomVariable(new CustomVariable("year", jahr.get().toString()), 4);
				request.setCustomTrackingParameter("4", jahr.get());
			}

			sendRequest(request);
		}
		catch(Exception locale) {
			locale.printStackTrace();
		}
	}
}
