package org.euv.trash.pdf.controller;

import java.io.IOException;
import java.util.Optional;

import jakarta.servlet.http.HttpServletRequest;

import org.euv.trash.pdf.PiwikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class PdfController {

	@Autowired(required = true)
	PdfGenerator pdfGenerator;
	
	@Autowired
	PiwikService piwikService;

	@GetMapping("/v1/{haId}")
	public ResponseEntity<Resource> generatePdfFile(@PathVariable(value = "haId", required = true)long haId, @RequestParam("jahr")Optional<Integer> reqJahr, HttpServletRequest request) {
		long time = System.currentTimeMillis();
		try {
			return pdfGenerator.generatePdfFile(haId, reqJahr, request);
		}
		catch(IOException e) {
			piwikService.logError(haId, reqJahr, e, System.currentTimeMillis() - time, request);
			return null;
		}
	}
}
