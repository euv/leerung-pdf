package org.euv.trash.pdf.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.function.Predicate;

import jakarta.servlet.http.HttpServletRequest;

import org.euv.trash.leerung.client.api.LeerungenApi;
import org.euv.trash.leerung.client.invoker.ApiClient;
import org.euv.trash.leerung.client.invoker.RFC3339DateFormat;
import org.euv.trash.leerung.client.model.LeerungenGenericLeerungsDate;
import org.euv.trash.leerung.client.model.LeerungsDate;
import org.euv.trash.pdf.AppConfig;
import org.euv.trash.pdf.Feiertag;
import org.euv.trash.pdf.FeiertageService;
import org.euv.trash.pdf.PiwikService;
import org.euv.trash.pdf.umweltbrummi.client.SubTourDates;
import org.euv.trash.pdf.umweltbrummi.client.Tour;
import org.euv.trash.pdf.umweltbrummi.client.UmweltbrummiApi;
import org.euv.trash.pdf.webapi.client.Planungstag;
import org.euv.trash.pdf.webapi.client.WebApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.annotation.RequestScope;

import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfViewerPreferences;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.itextpdf.svg.converter.SvgConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequestScope
public class PdfGenerator {
	
	@Autowired(required = false)
	BuildProperties buildProperties;
	@Autowired
	FeiertageService feiertageService;
	@Autowired
	AppConfig appConfig;
	@Autowired
	UmweltbrummiApi umweltbrummiApi;
	@Autowired
	WebApiClient webApiClient;
	@Autowired
	PiwikService piwikService;

	

	private static DeviceRgb COLOR_COL_HEADER = new DeviceRgb(191, 190, 188);
	private static DeviceRgb COLOR_FEIERTAG = new DeviceRgb(202, 237, 246);
	private static DeviceRgb COLOR_WEEKDAY = new DeviceRgb(213, 213, 209);
	private static DeviceRgb COLOR_AKTIONSTAG = new DeviceRgb(213, 213, 209);
	
	private static final String WEBAPI_TAG = "pdf";
	
	private static final String IMAGE_RESTABFALL = "restabfall";
	private static final String IMAGE_BIOABFALL = "bioabfall";
	private static final String IMAGE_WERTSTOFFE = "wertstoffe";
	private static final String IMAGE_ALTPAPIER = "altpapier";
	private static final String IMAGE_UBTOUR1 = "ubtour1";
	private static final String IMAGE_UBTOUR2 = "ubtour2";
	private static final String IMAGE_QRCODE = "qrcode-umweltbrummi";
	//private static final String IMAGE_TANNENBAUM = "tannenbaumsammlung";


	Style styleMonatsnamen = new Style();
	Style styleVersion = new Style();
	Style styleColHeader = new Style();
	Style styleTitle1 = new Style();
	Style styleTitle2 = new Style();
	Style styleTitle3 = new Style();
	Style styleTitle4 = new Style();
	Style styleFootLine = new Style();
	Style styleTonne = new Style();
	Style styleRow = new Style();
	Style[] styleWeekdays = {
			new Style(),
			new Style(),
			new Style(),
			new Style(),
			new Style(),
			new Style(),
			new Style()
	};
	Style styleFeiertag = new Style();
	Style styleFeiertagText = new Style();
	Style styleAktionstag = new Style();
	Style styleAktionstagText = new Style();
	Style styleBold = new Style();
	Style styleFont7 = new Style();
	Style styleFont8 = new Style();
	Style styleFont9 = new Style();
	Style stylePaddingRight = new Style();
	Style styleMid = new Style();
	Style stylePaddingTop5 = new Style();
	Style styleNoBorder = new Style();
	
	Style[] styleDebug = {
			new Style(),
			new Style(),
			new Style()
	};
	

	
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	PdfDocument pdfDoc;
	Document doc;
	Style styleNoPaddingMargins = new Style();
	int jahr;
	LeerungenGenericLeerungsDate leerungen;
	long haId;
	List<Feiertag> listFeiertages;
	List<LocalDate> listTour1 = new ArrayList<>();
	List<LocalDate> listTour2 = new ArrayList<>();


	
	public ResponseEntity<Resource> generatePdfFile(long haId, Optional<Integer> reqJahr, HttpServletRequest request) throws IOException {
		long time = System.currentTimeMillis();
		this.haId = haId;
		
		if(reqJahr.isEmpty()) {
			jahr = LocalDate.now().getYear();
		} else {
			jahr = reqJahr.get();
		}

		

		try {
			listFeiertages = feiertageService.getFeiertage(jahr);
		}
		catch(HttpClientErrorException hcee) {
			listFeiertages = new LinkedList<Feiertag>();
			log.error("Feiertage können nicht abgerufen werden");
		}
		log.debug("Liste aller Feiertage: {}", listFeiertages.toString());
		
		leerungen = getDataFromWebservice(haId);
		
		fillUmweltbrummiDates();

		ByteArrayOutputStream byos = new ByteArrayOutputStream();
		PdfWriter pdfWriter = new PdfWriter(byos);
		pdfDoc = new PdfDocument(pdfWriter);
		
		pdfDoc.setTagged();
		pdfDoc.getCatalog().setViewerPreferences(new PdfViewerPreferences().setDisplayDocTitle(true));
		pdfDoc.getCatalog().setLang(new PdfString("de-DE"));
		PdfDocumentInfo info = pdfDoc.getDocumentInfo();
		info.setAuthor("EUV Stadtbetrieb Castrop-Rauxel -AöR-");
		info.setTitle("Leerungstermine "+ Integer.toString(jahr) +" - "+ leerungen.getStrasse() +" "+ leerungen.getNummer());
		
		Set<String> s = FontProgramFactory.getRegisteredFonts();
		for (String string : s) {
			log.trace("Font registered: {}", string);
		}
		
		doc = new Document(pdfDoc, PageSize.A4.rotate());
		doc.setMargins(PdfHelper.mmInValue(10),PdfHelper.mmInValue(15),PdfHelper.mmInValue(0),PdfHelper.mmInValue(15));

		try {
			PdfFont pdfFontHelvetica = PdfFontFactory.createRegisteredFont("helvetica");
			log.trace("FONT as default: {}", pdfFontHelvetica);
			doc.setFont(pdfFontHelvetica);
		}
		catch(IOException ioe) {
			log.error("Could not register font");
		}

		initStyles();
		
		String[] monate1 = {
				"JANUAR",
				"FEBRUAR",
				"MÄRZ",
				"APRIL",
				"MAI",
				"JUNI"
		};
		log.debug("Begin render page 1");
		renderPage(monate1, 1);
		log.debug("End render page 1");

		doc.add(new AreaBreak(PageSize.A4.rotate()));
		String[] monate2 = {
				"JULI",
				"AUGUST",
				"SEPTEMBER",
				"OKTOBER",
				"NOVEMBER",
				"DEZEMBER"
		};
		log.debug("Begin render page 2");
		renderPage(monate2, 7);
		log.debug("End render page 2");

		doc.close();
		log.debug("Closed doc");
		
		piwikService.logCreatePdf(haId, leerungen.getStrasse() +" "+ leerungen.getNummer() +"("+ Long.toString(haId) +")", reqJahr, System.currentTimeMillis()-time, request);

		return createResponse(new ByteArrayInputStream(byos.toByteArray()), "EUV-Abfallkalender "+ Integer.toString(jahr) +" - "+ leerungen.getStrasse() +" "+ leerungen.getNummer());
	}

	private void fillUmweltbrummiDates() {
		for(Tour tour : umweltbrummiApi.getTour()) {
			Integer icon = tour.getIcon();
			if(icon>=1 && icon<=2) {
				for(UUID subTourId : tour.getSubTours()) {
					for(SubTourDates subTourDates : umweltbrummiApi.getSubTour(subTourId).getSubTourDates()) {
						if(icon==1) {
							listTour1.add(subTourDates.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
						} else if(icon==2) {
							listTour2.add(subTourDates.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
						}
					}
				}
			}
		}
		
		log.debug("SubTour1: {}", listTour1);
		log.debug("SubTour2: {}", listTour2);
	}

	private void initStyles() {
		styleNoPaddingMargins.setPadding(0);
		styleNoPaddingMargins.setMargin(0);
		styleNoPaddingMargins.setBorder(Border.NO_BORDER);

		styleMonatsnamen.setBold();

		styleColHeader.setBackgroundColor(COLOR_COL_HEADER);
		styleColHeader.setBorder(new SolidBorder(DeviceRgb.WHITE, PdfHelper.mmInValue(1)));

		styleTitle1.setBorder(Border.NO_BORDER);
		styleTitle1.setFontSize(11.0f);
		styleTitle1.setBold();

		styleTitle2.setBorder(Border.NO_BORDER);
		styleTitle2.setFontSize(7.0f);

		styleTitle3.setBorder(Border.NO_BORDER);
		styleTitle3.setFontSize(9.0f);
		styleTitle3.setBold();

		styleTitle4.setBorder(Border.NO_BORDER);
		styleTitle4.setFontSize(7.0f);
		
		styleFootLine.setMargin(0);
		styleFootLine.setPadding(0);
		styleFootLine.setFontSize(7.0f);
		
		styleVersion.setMargin(0);
		styleVersion.setPadding(0);
		styleVersion.setFontSize(5.0f);
		styleVersion.setBorder(Border.NO_BORDER);
		
		styleTonne.setPadding(0);
		styleTonne.setMargin(0);
		styleTonne.setMarginRight(PdfHelper.mmInValue(2));
		styleTonne.setBorder(Border.NO_BORDER);

		Border borderBlack = new SolidBorder(DeviceRgb.BLACK, PdfHelper.mmInValue(0.1f));
		Border borderWhite = new SolidBorder(DeviceRgb.WHITE, PdfHelper.mmInValue(0.5f));
		styleRow.setFontSize(9.0f);
		styleRow.setBackgroundColor(DeviceRgb.WHITE);
		styleRow.setPadding(0);
		styleRow.setMargin(0);
		styleRow.setPaddingLeft(5);
		styleRow.setPaddingRight(5);
		styleRow.setPaddingBottom(-1);
		styleRow.setHeight(PdfHelper.mmInValue(5.0f));
		//styleRow.setBorder(Border.NO_BORDER);
		styleRow.setBorderBottom(borderBlack);
		styleRow.setBorderTop(borderBlack);
		styleRow.setBorderLeft(borderWhite);
		styleRow.setBorderRight(borderWhite);

		styleRow.setVerticalAlignment(com.itextpdf.layout.properties.VerticalAlignment.BOTTOM);
		
		styleWeekdays[6-1].setBackgroundColor(COLOR_WEEKDAY);
		styleWeekdays[7-1].setBackgroundColor(COLOR_WEEKDAY);

		styleFeiertag.setBackgroundColor(COLOR_FEIERTAG);
		
		styleFeiertagText.setFontSize(6.0f);
		styleFeiertagText.setMargin(0);
		styleFeiertagText.setPadding(0);
		
		styleAktionstag.setBackgroundColor(COLOR_AKTIONSTAG);
		styleAktionstagText.setFontSize(6.0f);

		styleTonne.setBorder(Border.NO_BORDER);
		styleTonne.setMargin(0);
		styleTonne.setPadding(0);
		styleTonne.setPaddingRight(PdfHelper.mmInValue(1));
		styleTonne.setMarginBottom(0);
		styleTonne.setMarginRight(PdfHelper.mmInValue(1.0f));
		styleTonne.setVerticalAlignment(VerticalAlignment.MIDDLE);

		styleBold.setBold();
		styleFont7.setFontSize(7.0f);
		styleFont8.setFontSize(8.0f);
		styleFont9.setFontSize(9.0f);
		
		stylePaddingTop5.setPaddingTop(PdfHelper.mmInValue(5.0f));
		stylePaddingRight.setPaddingRight(PdfHelper.mmInValue(5.0f));
		//stylePaddingRight.setMarginRight(PdfHelper.mmInValue(5.0f));
		
		styleDebug[0].setBackgroundColor(DeviceRgb.GREEN);
		styleDebug[1].setBackgroundColor(DeviceRgb.RED);
		styleDebug[2].setBackgroundColor(DeviceRgb.BLUE);
		
		styleMid.setVerticalAlignment(VerticalAlignment.MIDDLE);
		
		styleNoBorder.setBorder(Border.NO_BORDER);
	}

	private ResponseEntity<Resource> createResponse(ByteArrayInputStream bais, String filename) {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename="+ filename +".pdf");
		headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
		headers.add(HttpHeaders.PRAGMA, "no-cache");
		headers.add(HttpHeaders.EXPIRES, "0");

		Resource body = new ByteArrayResource(bais.readAllBytes());
		
		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(body);
	}

	private LeerungenGenericLeerungsDate getDataFromWebservice(long haId) {
		ApiClient apiClient = new ApiClient();
		apiClient.setBasePath(appConfig.getLeerungBaseUrl());
		RFC3339DateFormat df = new RFC3339DateFormat();
		df.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
		apiClient.setDateFormat(df);
		
		LeerungenApi leerungenApi = new LeerungenApi(apiClient);
		LeerungenGenericLeerungsDate leerungen = leerungenApi.getLeerungFeiertagsverschiebungByHaId2(haId, true);
		return leerungen;
	}

	private void renderPage(String[] monate, int start) throws IOException {
		float width = doc.getPageEffectiveArea(PageSize.A4.rotate()).getWidth();
		float cellWidth = width / 6;

		doc.add(createPageHeaderLine());

		// First row (header)
		Table table = new Table(new float[] {cellWidth,cellWidth,cellWidth,cellWidth,cellWidth,cellWidth});
		for(String m : monate) {
			Text text = new Text(m);
			text.addStyle(styleMonatsnamen);
			
			Cell c = new Cell().add(new Paragraph(text));
			//c.setBorder(new SolidBorder(1.0f));
			c.addStyle(styleColHeader);
			c.setTextAlignment(TextAlignment.CENTER);
			table.addCell(c);
		}
		
		// Data in cols
		for(int m=start; m<start+6; ++m) {
			Table tableMonth = renderMonth(m, cellWidth);
			
			Cell c = new Cell();
			c.addStyle(styleNoPaddingMargins);
			c.add(tableMonth);
			
			table.addCell(c);
		}
		doc.add(table);
		
		doc.add(createPageFooterLine());
	}

	private Cell createPageHeaderLine() throws IOException {
		Cell c = new Cell();
		c.addStyle(styleNoPaddingMargins);
		c.setHeight(PdfHelper.mmInValue(13.0f));
		
		TabStop[] tabStops = {
				new TabStop(PdfHelper.mmInValue(45.0f), TabAlignment.LEFT),
				new TabStop(PdfHelper.mmInValue(50.0f), TabAlignment.LEFT)
		};
		Paragraph p = new Paragraph();
		p.addTabStops(tabStops);
		
		Cell imgLogo = createImageCellSvg("euv", 100.0f, 9.7f);
		imgLogo.addStyle(styleMid);
		p.add(imgLogo);
		
		Text textTitle1 = new Text("ABFALLKALENDER "+ Integer.toString(jahr));
		textTitle1.addStyle(styleTitle1);
		
		Text textTitle2 = new Text("Verschiebung der Leerungstermine durch Feiertage sind berücksichtigt.");
		textTitle2.addStyle(styleTitle2);
		
		Cell cellText1 = new Cell();
		cellText1.addStyle(stylePaddingRight);
		cellText1.add(new Paragraph(textTitle1));
		cellText1.add(new Paragraph(textTitle2));
		p.add(new Tab());
		p.add(cellText1);

		
		
		Text textTitle3 = new Text("IHR STANDORT: "+ leerungen.getStrasse() +" "+ leerungen.getNummer());
		textTitle3.addStyle(styleTitle3);
		
		LocalDate ld = LocalDate.now();
		Text textTitle4 = new Text("Stand: "+ ld.format(dtf) +" / Bitte überprüfen Sie regelmäßig Ihre Abfuhrtermine!");
		textTitle4.addStyle(styleTitle4);
		
		Cell cellText2 = new Cell();
		cellText2.addStyle(stylePaddingRight);
		cellText2.add(new Paragraph(textTitle3));
		cellText2.add(new Paragraph(textTitle4));
		p.add(cellText2);

		
		
		Text textTitle5 = new Text("Sie haben Fragen? ");
		Text textTitle6 = new Text("Tel. ");
		Text textTitle7 = new Text("02305 9686 - 666");
		Text textTitle8 = new Text("E-Mail: ");
		Text textTitle9 = new Text("abfallwirtschaft@euv-stadtbetrieb.de");
		textTitle5.addStyle(styleFont8);
		textTitle5.addStyle(styleBold);
		textTitle6.addStyle(styleFont7);
		textTitle6.addStyle(styleBold);
		textTitle7.addStyle(styleFont7);
		textTitle8.addStyle(styleFont7);
		textTitle8.addStyle(styleBold);
		textTitle9.addStyle(styleFont7);
		
		Cell cellFragen = new Cell();
		Paragraph p1 = new Paragraph();
		p1.add(textTitle5);
		p1.add(textTitle6);
		p1.add(textTitle7);
		cellFragen.add(p1);
		
		Paragraph p2 = new Paragraph();
		p2.add(textTitle8);
		p2.add(textTitle9);
		cellFragen.add(p2);

		p.add(cellFragen);

		c.add(p);
		return c;
	}
	
	
	
	private Cell createPageFooterLine() throws IOException {
		Cell cell1 = createFootCell1();
		Style styleCell1 = new Style();
		styleCell1.setPaddingLeft(PdfHelper.mmInValue(5.0f));
		styleCell1.setBorder(Border.NO_BORDER);
		cell1.addStyle(styleCell1);

		Cell cell2 = createFootCell2();
		Style styleCell2 = new Style();
		styleCell2.setBorder(Border.NO_BORDER);
		cell2.addStyle(styleCell2);
		
		Cell cell3 = createFootCell3();
		Style styleCell3 = new Style();
		styleCell3.setBorder(Border.NO_BORDER);
		cell3.addStyle(styleCell3);

		/* Add version info */
		Cell cellVersion = new Cell();
		cellVersion.addStyle(styleNoPaddingMargins);
		cellVersion.setTextAlignment(TextAlignment.RIGHT);
		Paragraph pVersion = new Paragraph();
		pVersion.addStyle(styleVersion);
		String versionText = "Leerung-PDF Version "+ (buildProperties==null ? "DEV" : buildProperties.getVersion());
		log.trace("Version text: {}", versionText);
		Text textVersion = new Text(versionText);
		pVersion.add(textVersion);
		cellVersion.add(pVersion);
		
		UnitValue[] unitTable = {
				new UnitValue(UnitValue.PERCENT, 34),
				new UnitValue(UnitValue.PERCENT, 33),
				new UnitValue(UnitValue.PERCENT, 33)
		};
		Table tableFootline = new Table(unitTable);
		tableFootline.addCell(cell1);
		tableFootline.addCell(cell2);
		tableFootline.addCell(cell3);
		tableFootline.useAllAvailableWidth();
		
		Cell cellFootline = new Cell();
		cellFootline.addStyle(styleNoPaddingMargins);
		cellFootline.setPaddingTop(PdfHelper.mmInValue(2));
		cellFootline.add(tableFootline);

		return cellFootline;
	}
	
	private Cell createFootCell3() throws MalformedURLException {
		Cell cell = new Cell();
		cell.addStyle(styleFootLine);
		cell.addStyle(styleNoBorder);
		cell.setPaddingLeft(PdfHelper.mmInValue(5.0f));
		
		StringBuilder stringBuilder = new StringBuilder();
		LocalDate ld = LocalDate.now();
		ld = ld.plusYears(jahr-ld.getYear());
		log.debug("Query Planungstag at {}", ld.toString());
		List<Planungstag> planungstags = Arrays.asList(webApiClient.getPlanungstagByTagAndDate(WEBAPI_TAG, ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
		Iterator<Planungstag> iterator = planungstags.iterator();
		while(iterator.hasNext()) {
			Planungstag planungstag = iterator.next();
			stringBuilder.append(planungstag.getBeschreibung());
			if(iterator.hasNext()) {
				stringBuilder.append(", ");
			}
		}

		Cell part1 = new Cell();
		part1.add(new Paragraph(stringBuilder.toString()));
		part1.addStyle(styleNoBorder);

		Cell part2 = new Cell();
		part2.add(new Paragraph("Alles im Blick unter: www.euv-stadtbetrieb.de"));
		
		cell.add(part1);		
		cell.add(part2);
		cell.addStyle(styleMid);
		return cell;
	}

	private Cell createFootCell2() throws IOException {
		Cell cell = new Cell();
		cell.addStyle(styleFootLine);
		cell.addStyle(styleNoBorder);
		
		Table table = new Table(2);
		
		Cell text = new Cell();
		text.add(new Paragraph("Zusätzlich steht der Umweltbrummi jeden Samstag am Bringhof Ickern. Für Infos zur Entsorgung und der Sammeltouren, einfach den QR-Code scannen!"));
		text.addStyle(styleNoBorder);
		text.addStyle(styleMid);
		table.addCell(text);
		
		Cell image = createImageCellSvg(IMAGE_QRCODE, 15.0f, 15.0f);
		image.addStyle(styleNoBorder);
		image.addStyle(styleMid);
		table.addCell(image);
		
		cell.add(table);
		return cell;
	}

	private Cell createFootCell1() throws IOException {
		Cell cellLeft1 = new Cell();
		
		Table tableImages = new Table(8);
		tableImages.addStyle(styleFootLine);
		tableImages.addStyle(styleNoBorder);
		
		createFootcellPart(tableImages, IMAGE_RESTABFALL, "Restabfall");
		tableImages.addCell(createSpaceCell(5.0f, styleNoBorder));
		createFootcellPart(tableImages, IMAGE_WERTSTOFFE, "Wertstoffe");
		tableImages.addCell(createSpaceCell(5.0f, styleNoBorder));
		createFootcellPart(tableImages, IMAGE_UBTOUR1, "Tour 1 / Umweltbrummi");

		createFootcellPart(tableImages, IMAGE_BIOABFALL, "Bioabfall");
		tableImages.addCell(createSpaceCell(5.0f, styleNoBorder));
		createFootcellPart(tableImages, IMAGE_ALTPAPIER, "Altpapier");
		tableImages.addCell(createSpaceCell(5.0f, styleNoBorder));
		createFootcellPart(tableImages, IMAGE_UBTOUR2, "Tour 2 / Umweltbrummi");

		cellLeft1.add(tableImages);
		cellLeft1.addStyle(styleMid);
		return cellLeft1;
	}

	private void createFootcellPart(Table tableImages, String image, String text) throws IOException {
		Cell imageRestabfall = createImageCellSvg(image, 3.0f, 3.0f);
		imageRestabfall.addStyle(styleTonne);
		imageRestabfall.addStyle(styleMid);
		imageRestabfall.addStyle(styleNoBorder);
		Paragraph textRestabfall = new Paragraph(text);
		Cell cellRestabfall = new Cell();
		cellRestabfall.add(textRestabfall);
		cellRestabfall.addStyle(styleMid);
		cellRestabfall.addStyle(styleNoBorder);
		tableImages.addCell(imageRestabfall);
		tableImages.addCell(cellRestabfall);
	}
	
	
	/*
	private Text createTermine() {
		return new Text(terminControllerApi.listByTypUsingGET1("FZ")
				.stream()
				.map( x -> x.getBezeichnung())
				.collect(Collectors.joining(" / ")));
	}
	*/
	
	private Table renderMonth(int month, float cellWidth) throws IOException {
		String[] daynames = {
				"Mo",
				"Di",
				"Mi",
				"Do",
				"Fr",
				"Sa",
				"So"
		};
		

		Table table = new Table(1);
		//table.setBorderCollapse(BorderCollapsePropertyValue.COLLAPSE);
		table.setBorder(Border.NO_BORDER);
		table.useAllAvailableWidth();
		TabStop[] tabStops = {
				new TabStop(15, TabAlignment.RIGHT),
				new TabStop(20, TabAlignment.LEFT),
				new TabStop(42, TabAlignment.LEFT),
				new TabStop(cellWidth, TabAlignment.RIGHT),
		};
		/*
		 * Messup code... Ugly, but working!
		 * Must be refactored
		 */
		Image imageRestabfall = createSvgImage(IMAGE_RESTABFALL, 3.0f, 3.0f);
		imageRestabfall.addStyle(styleTonne);
		Image imageWertstoffe = createSvgImage(IMAGE_WERTSTOFFE, 3.0f, 3.0f);
		imageWertstoffe.addStyle(styleTonne);
		Image imageBioabfall = createSvgImage(IMAGE_BIOABFALL, 3.0f, 3.0f);
		imageBioabfall.addStyle(styleTonne);
		Image imageAltpapier = createSvgImage(IMAGE_ALTPAPIER, 3.0f, 3.0f);
		imageAltpapier.addStyle(styleTonne);

		Image imageTour1 = createSvgImage(IMAGE_UBTOUR1, 3.0f, 3.0f);
		imageTour1.addStyle(styleTonne);
		Image imageTour2 = createSvgImage(IMAGE_UBTOUR2, 3.0f, 3.0f);
		imageTour2.addStyle(styleTonne);

		for(int d=1; d<32; ++d) {
			try {
				Cell c = new Cell();
				c.addStyle(styleRow);
				
				LocalDate ld = LocalDate.of(jahr, month, d);
				log.trace("LD: {}", ld);

				Text dayname = new Text(daynames[ld.getDayOfWeek().getValue()-1]);
				Text day = new Text(String.format("%02d", d));
				day.addStyle(styleBold);
				
				c.addStyle(styleWeekdays[ld.getDayOfWeek().getValue()-1]);
				
				Paragraph p = new Paragraph();
				p.addTabStops(tabStops);
				
				p.add(new Tab()).add(day).add(new Tab()).add(dayname);
				p.add(new Tab());
				
				
				
				Predicate<LeerungsDate> matchLeerungsDate = x -> x.getLeerung().isEqual(ld);
				if(leerungen.getGrau().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageRestabfall);
				}
				if(leerungen.getGelb().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageWertstoffe);
				}
				if(leerungen.getBraun().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageBioabfall);
				}
				if(leerungen.getBlau().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageAltpapier);
				}
				
				if(listTour1.stream().anyMatch(x -> ld.isEqual(x))) {
					p.add(imageTour1);
				};
				if(listTour2.stream().anyMatch(x -> ld.isEqual(x))) {
					p.add(imageTour2);
				};
				/*
				createAktionTermin(styleAktionstag, styleAktionstagText, styleTonne, p, c, ld);
				*/
				createFeiertagTermin(styleFeiertag, styleFeiertagText, p, c, ld);
			
				c.add(p);

				//c.setNextRenderer(new UpDownCellRenderer(c));
				table.addCell(c);
			}
			catch(DateTimeException dte) {
				log.trace("DateTimeException catched: {}", dte.toString());
				Cell c = new Cell();
				c.addStyle(styleRow);
				table.addCell(c);
			}
		}
		
		return table;
	}

	private void createFeiertagTermin(Style styleFeiertag, Style styleFeiertagText, Paragraph p, Cell c, LocalDate ld) {
		Feiertag feiertag = listFeiertages.stream().filter(x -> x.getDatum().isEqual(ld)).reduce(null, (a,b) -> b);
		if(feiertag !=  null) {
			Text t = new Text(feiertag.getName() == null ? "" : feiertag.getName().substring(0, feiertag.getName().length()>21 ? 21 : feiertag.getName().length()));
			t.addStyle(styleFeiertagText);
			p.add(t);
			
			c.addStyle(styleFeiertag);
		}
	}
	
	/*
	private void createAktionTermin(Style styleAktionstag, Style styleAktionstagText, Style styleTonne, Paragraph p, Cell c, LocalDate ld) throws MalformedURLException {
		Termin terminAktionstag = listAktionstage.stream().filter(x -> LocalDate.parse(x.getDatum()).isEqual(ld)).reduce(null, (a,b) -> b);
		if(terminAktionstag != null) {
			try {
				Image image = createImage(terminAktionstag.getIcon());
				image.addStyle(styleTonne);
				p.add(image);
			}
			catch(com.itextpdf.io.IOException ioe) {
				logger.debug("Catch exception for ({})", terminAktionstag.getIcon());
			}
			Text t = new Text(terminAktionstag.getBezeichnung() == null ? "" : terminAktionstag.getBezeichnung());
			t.addStyle(styleAktionstagText);
			p.add(t);

			c.addStyle(styleAktionstag);
		}
	}
	*/
	
	private Image createSvgImage(String name, float width, float height) throws IOException {
		try {
			Resource resource = new ClassPathResource("images/"+ name +".svg");
			InputStream inputStream = resource.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(inputStream);
			
			Image image = SvgConverter.convertToImage(bis, pdfDoc);
			image.setAutoScale(false);
			image.scaleToFit(PdfHelper.mmInValue(width), PdfHelper.mmInValue(height));
/*			image.setWidth(PdfHelper.mmInValue(width));
			image.setHeight(PdfHelper.mmInValue(height));*/
			bis.close();
			inputStream.close();
			return image;		
		}
		catch(IOException ioe) {
			log.error("Exception in createSvgImage", ioe);
			throw ioe;
		}
	}
/*
	private Image createSvgImage(String name, float height) throws IOException {
		try {
			Resource resource = new ClassPathResource("images/"+ name +".svg");
			InputStream inputStream = resource.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(inputStream);
			
			Image image = SvgConverter.convertToImage(bis, pdfDoc);
			image.setAutoScale(false);
			image.scaleToFit(-1, PdfHelper.mmInValue(height));
			//image.setHeight(PdfHelper.mmInValue(height));
			bis.close();
			inputStream.close();
			return image;		
		}
		catch(IOException ioe) {
			log.error("Exception in createSvgImage", ioe);
			throw ioe;
		}
	}
*/
	private Cell createImageCellSvg(String name, float width, float height) throws IOException {
		Cell c = new Cell();
		c.add(createSvgImage(name, width, height));
		return c;
	}
/*
	private Cell createImageCellSvg(String name, float height) throws IOException {
		Cell c = new Cell();
		c.add(createSvgImage(name, height));
		return c;
	}
*/
	private Cell createSpaceCell(float mm, Style style) {
		Cell cellSpace = new Cell();
		if(style!=null) {
			cellSpace.addStyle(style);
		}
		cellSpace.setWidth(PdfHelper.mmInValue(mm));
		return cellSpace;
	}
}
