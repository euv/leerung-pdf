package org.euv.trash.pdf.webapi.client;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.euv.trash.pdf.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class WebApiClient {
	@Autowired
	AppConfig appConfig;
	
	public Planungstag[] getPlanungstagByTag(String tag) {
		RestTemplate restTemplate = new RestTemplateBuilder().build();
		restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(appConfig.getWebapiBaseUrl()));

		Planungstag[] planungstags = null;
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("tag", tag);
		ResponseEntity<Planungstag[]> result = restTemplate.getForEntity("/v1/planungstag?tag={tag}", Planungstag[].class, params);
		log.debug("getPlanungstagByTag status: {}", result.getStatusCodeValue());
		if(result.getStatusCode()==HttpStatus.OK) {
			planungstags = result.getBody();
		}
		
		return planungstags;
	}
	
	public Planungstag[] getPlanungstagByTagAndDate(String tag, Instant datum) {
		Planungstag[] planungstags = getPlanungstagByTag(tag);
		ArrayList<Planungstag> list = new ArrayList<Planungstag>();

		for(Planungstag planungstag : planungstags) {
			Instant start = planungstag.getDatumStart().toInstant();
			Instant end = planungstag.getDatumEnde().toInstant();
			log.debug("Planungstag dates {}: {}/{}", datum, start, end);
			
			if((datum.isAfter(start) || datum.equals(start)) && datum.isBefore(end)) {
				list.add(planungstag);
			}
		}
		
		return list.toArray(new Planungstag[list.size()]);
	}
}
