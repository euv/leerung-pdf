package org.euv.trash.pdf.webapi.client;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Planungstag {
	String id;
	String beschreibung;
	String abkuerzung;
	Date datum;
	Date datumStart;
	Date datumEnde;
	String[] tags;
}
