/*
 * Leerungsabfrage REST API
 * API für die Abfrage von Leerungen an Häusern in Castrop-Rauxel
 *
 * OpenAPI spec version: 0.2.1-oas3
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package org.euv.trash.leerung.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
/**
 * Information zu Leerungen für ein Haus
 */
@Schema(description = "Information zu Leerungen für ein Haus")
@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-12-14T08:17:54.663Z[GMT]")
public class LeerungenGenericLocalDate {
  @JsonProperty("haId")
  private Long haId = null;

  @JsonProperty("stId")
  private Long stId = null;

  @JsonProperty("strasse")
  private String strasse = null;

  @JsonProperty("nummer")
  private String nummer = null;

  @JsonProperty("gelb")
  private List<LocalDate> gelb = new ArrayList<>();

  @JsonProperty("braun")
  private List<LocalDate> braun = new ArrayList<>();

  @JsonProperty("grau")
  private List<LocalDate> grau = new ArrayList<>();

  @JsonProperty("blau")
  private List<LocalDate> blau = new ArrayList<>();

  public LeerungenGenericLocalDate haId(Long haId) {
    this.haId = haId;
    return this;
  }

   /**
   * Die Haus ID
   * @return haId
  **/
  @Schema(description = "Die Haus ID")
  public Long getHaId() {
    return haId;
  }

  public void setHaId(Long haId) {
    this.haId = haId;
  }

  public LeerungenGenericLocalDate stId(Long stId) {
    this.stId = stId;
    return this;
  }

   /**
   * Die Straßen ID
   * @return stId
  **/
  @Schema(description = "Die Straßen ID")
  public Long getStId() {
    return stId;
  }

  public void setStId(Long stId) {
    this.stId = stId;
  }

  public LeerungenGenericLocalDate strasse(String strasse) {
    this.strasse = strasse;
    return this;
  }

   /**
   * Der Straßenname
   * @return strasse
  **/
  @Schema(description = "Der Straßenname")
  public String getStrasse() {
    return strasse;
  }

  public void setStrasse(String strasse) {
    this.strasse = strasse;
  }

  public LeerungenGenericLocalDate nummer(String nummer) {
    this.nummer = nummer;
    return this;
  }

   /**
   * Die Hausnummer
   * @return nummer
  **/
  @Schema(description = "Die Hausnummer")
  public String getNummer() {
    return nummer;
  }

  public void setNummer(String nummer) {
    this.nummer = nummer;
  }

  public LeerungenGenericLocalDate gelb(List<LocalDate> gelb) {
    this.gelb = gelb;
    return this;
  }

  public LeerungenGenericLocalDate addGelbItem(LocalDate gelbItem) {
    this.gelb.add(gelbItem);
    return this;
  }

   /**
   * Array mit den Leerung für Wertstoffe
   * @return gelb
  **/
  @Schema(required = true, description = "Array mit den Leerung für Wertstoffe")
  public List<LocalDate> getGelb() {
    return gelb;
  }

  public void setGelb(List<LocalDate> gelb) {
    this.gelb = gelb;
  }

  public LeerungenGenericLocalDate braun(List<LocalDate> braun) {
    this.braun = braun;
    return this;
  }

  public LeerungenGenericLocalDate addBraunItem(LocalDate braunItem) {
    this.braun.add(braunItem);
    return this;
  }

   /**
   * Array mit den Leerung für die Biotonne
   * @return braun
  **/
  @Schema(required = true, description = "Array mit den Leerung für die Biotonne")
  public List<LocalDate> getBraun() {
    return braun;
  }

  public void setBraun(List<LocalDate> braun) {
    this.braun = braun;
  }

  public LeerungenGenericLocalDate grau(List<LocalDate> grau) {
    this.grau = grau;
    return this;
  }

  public LeerungenGenericLocalDate addGrauItem(LocalDate grauItem) {
    this.grau.add(grauItem);
    return this;
  }

   /**
   * Array mit den Leerung für Restmüll
   * @return grau
  **/
  @Schema(required = true, description = "Array mit den Leerung für Restmüll")
  public List<LocalDate> getGrau() {
    return grau;
  }

  public void setGrau(List<LocalDate> grau) {
    this.grau = grau;
  }

  public LeerungenGenericLocalDate blau(List<LocalDate> blau) {
    this.blau = blau;
    return this;
  }

  public LeerungenGenericLocalDate addBlauItem(LocalDate blauItem) {
    this.blau.add(blauItem);
    return this;
  }

   /**
   * Array mit den Leerung für die Papiertonne
   * @return blau
  **/
  @Schema(required = true, description = "Array mit den Leerung für die Papiertonne")
  public List<LocalDate> getBlau() {
    return blau;
  }

  public void setBlau(List<LocalDate> blau) {
    this.blau = blau;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LeerungenGenericLocalDate leerungenGenericLocalDate = (LeerungenGenericLocalDate) o;
    return Objects.equals(this.haId, leerungenGenericLocalDate.haId) &&
        Objects.equals(this.stId, leerungenGenericLocalDate.stId) &&
        Objects.equals(this.strasse, leerungenGenericLocalDate.strasse) &&
        Objects.equals(this.nummer, leerungenGenericLocalDate.nummer) &&
        Objects.equals(this.gelb, leerungenGenericLocalDate.gelb) &&
        Objects.equals(this.braun, leerungenGenericLocalDate.braun) &&
        Objects.equals(this.grau, leerungenGenericLocalDate.grau) &&
        Objects.equals(this.blau, leerungenGenericLocalDate.blau);
  }

  @Override
  public int hashCode() {
    return Objects.hash(haId, stId, strasse, nummer, gelb, braun, grau, blau);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LeerungenGenericLocalDate {\n");
    
    sb.append("    haId: ").append(toIndentedString(haId)).append("\n");
    sb.append("    stId: ").append(toIndentedString(stId)).append("\n");
    sb.append("    strasse: ").append(toIndentedString(strasse)).append("\n");
    sb.append("    nummer: ").append(toIndentedString(nummer)).append("\n");
    sb.append("    gelb: ").append(toIndentedString(gelb)).append("\n");
    sb.append("    braun: ").append(toIndentedString(braun)).append("\n");
    sb.append("    grau: ").append(toIndentedString(grau)).append("\n");
    sb.append("    blau: ").append(toIndentedString(blau)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
