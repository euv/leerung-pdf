package org.euv.trash.leerung.client.api;

import org.euv.trash.leerung.client.invoker.ApiClient;

import org.euv.trash.leerung.client.model.Hausnummer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-12-14T08:17:54.663Z[GMT]")@Component("org.euv.trash.leerung.client.api.HausnummernApi")
public class HausnummernApi {
    private ApiClient apiClient;

    public HausnummernApi() {
        this(new ApiClient());
    }

    @Autowired
    public HausnummernApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Gibt die Informationen zu einem Haus/Hausnummer zurück
     * 
     * <p><b>200</b> - Abfrage erfolgreich
     * <p><b>400</b> - Abfrage war nicht korrekt
     * <p><b>404</b> - Die Hausnummer wurde nicht gefunden.
     * <p><b>500</b> - Ein Fehler mit der Datenbank ist aufgetretten.
     * @param haId Die Haus ID
     * @param full Sollen die Straßenwerte mit zurück gegeben werden
     * @return Hausnummer
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Hausnummer getHausById1(Long haId, Boolean full) throws RestClientException {
        Object postBody = null;
        // verify the required parameter 'haId' is set
        if (haId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'haId' when calling getHausById1");
        }
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("haId", haId);
        String path = UriComponentsBuilder.fromPath("/v3/haus/{haId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "full", full));

        final String[] accepts = { 
            "application/json"
         };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Hausnummer> returnType = new ParameterizedTypeReference<Hausnummer>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Liste aller Häuser/Hausnummern auf einer Straße
     * Listet alle Häuser mit Hausnummern auf einer Straße auf
     * <p><b>200</b> - Erfolgreiche Abfrage
     * <p><b>400</b> - Abfrage war nicht korrekt
     * <p><b>404</b> - Auf der Straße wurden keine Hausnummern gefunden.
     * <p><b>500</b> - Ein Fehler mit der Datenbank ist aufgetretten.
     * @param stId Die Straßen ID
     * @param full Sollen die Straßenwerte mit zurück gegeben werden
     * @return List&lt;Hausnummer&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<Hausnummer> listAllHausByStId1(Long stId, Boolean full) throws RestClientException {
        Object postBody = null;
        // verify the required parameter 'stId' is set
        if (stId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'stId' when calling listAllHausByStId1");
        }
        String path = UriComponentsBuilder.fromPath("/v3/haus").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "stId", stId));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "full", full));

        final String[] accepts = { 
            "application/json"
         };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<Hausnummer>> returnType = new ParameterizedTypeReference<List<Hausnummer>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
