package org.euv.trash.leerung.client.api;

import org.euv.trash.leerung.client.invoker.ApiClient;

import org.euv.trash.leerung.client.model.InlineResponse400;
import org.euv.trash.leerung.client.model.Strasse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@jakarta.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-12-14T08:17:54.663Z[GMT]")@Component("org.euv.trash.leerung.client.api.StraenverzeichnisApi")
public class StraenverzeichnisApi {
    private ApiClient apiClient;

    public StraenverzeichnisApi() {
        this(new ApiClient());
    }

    @Autowired
    public StraenverzeichnisApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Informationen zu einer Straßen ID abrufen
     * 
     * <p><b>400</b> - Abfrage war nicht korrekt
     * <p><b>404</b> - Eine Straße mit dieser ID ist nicht vorhanden
     * <p><b>500</b> - Ein Fehler mit der Datenbank ist aufgetretten
     * @param stId The stId parameter
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public void getStrasseById1(Long stId) throws RestClientException {
        Object postBody = null;
        // verify the required parameter 'stId' is set
        if (stId == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'stId' when calling getStrasseById1");
        }
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("stId", stId);
        String path = UriComponentsBuilder.fromPath("/v3/strasse/{stId}").buildAndExpand(uriVariables).toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
         };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Void> returnType = new ParameterizedTypeReference<Void>() {};
        apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Liste aller Straßen im Stadtgebiet
     * Die Abfrage kann über verschiedene Parameter eingegenzt werden.
     * <p><b>200</b> - Erfolgreiche Abfrage
     * <p><b>400</b> - Abfrage war nicht korrekt
     * <p><b>404</b> - Ein Fehler mit der Datenbank ist aufgetretten
     * @param size Die Anzahl der zurückgegebenden Ergebnisse
     * @param search Ein Suchstring nach dem gesucht werden soll
     * @param part Soll als Teilstring gesucht werden
     * @return List&lt;Strasse&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<Strasse> listStrassen1(Integer size, String search, Boolean part) throws RestClientException {
        Object postBody = null;
        String path = UriComponentsBuilder.fromPath("/v3/strasse").build().toUriString();
        
        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "size", size));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "search", search));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "part", part));

        final String[] accepts = { 
            "application/json"
         };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<Strasse>> returnType = new ParameterizedTypeReference<List<Strasse>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
