# PDF interface for EUV Leerungen
This interface is for the PDF export of the wast collection.

# Link
The offical link is:

https://api.euv-stadtbetrieb.de/leerung-pdf/v1/{ID}

Where ID is the id of the house in our system.

# Parameter
One parameter is accepted. `jahr` -> year for which the calendar should
generated (eg jahr=2022).

# Contacts
marcus.schneider@euv-stadtbetrieb.de
